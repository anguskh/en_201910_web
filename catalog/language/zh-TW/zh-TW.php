<?php
// Locale
$_['code']                  = 'zh-TW';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'Y/m/d';
$_['date_format_long']      = 'Y F dS l';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'Y/m/d H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

// Text
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_yes']              = '是';
$_['text_no']               = '否';
$_['text_none']             = ' --- 無 --- ';
$_['text_select']           = ' --- 請選擇 --- ';
$_['text_all_zones']        = '所有地區';
$_['text_pagination']       = '第 %d ~ %d 筆，共 %d 筆資料';
$_['text_loading']          = '載入中...';

// Buttons
$_['button_back']           = '返回';
$_['button_cart']           = '加入購物車';
$_['button_cancel']         = '取消';
$_['button_checkout']       = '結帳';
$_['button_confirm']        = '確定送出';
$_['button_delete']         = '刪除';
$_['button_download']       = '下載';
$_['button_edit']           = '修改';
$_['button_filter']         = '篩選';
$_['button_login']          = '登入';
$_['button_update']         = '更新';
$_['button_remove']         = '移除';
$_['button_search']         = '搜尋';
$_['button_submit']         = '送出';