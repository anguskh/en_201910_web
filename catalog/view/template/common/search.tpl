<?php echo $header; ?>
    <div class="wrapper">
		<div class="header">
            <?php echo $header_bar; ?>
        </div>
		<div class="content searchAlbum">
			<div class="contentTop">
                <ul class="breadCrumb">
					<li><a href="https://www.eventpal.com.tw">首頁</a></li>
					<li><a href="index.php">相片咖</a></li>
                    <?php if(!empty($search)):?>
					<li><?php echo $search;?></li>
                    <?php endif;?>
				</ul>
                <div class="searchWrap">
                    <div class="searchBox">
                        <form action="<?php echo $search_action;?>" method="get" id="searchForm" class="searchForm clearfix" >
                            <input type="search" name="search" id="search" placeholder="搜尋華麗身影" results="">
                            <a class="searchKeyword" href="#" onclick="$('#searchForm').submit();">
                                <div class="btnSearch"></div>
                            </a>
                        </form>
                    </div>
                </div>
            </div>
            <?php if(!empty($search)):?>
            <div class="resultWrap">
				<div class="result">
					<div class="iconNoResult"></div>
					<h3 class="resultMsg">
                        <?php if(count($albums)):?>
                        相簿：『<?php echo $search;?>』，共有<?php echo count($albums);?>筆搜尋結果
                        <?php else:?>
                        "<span class="keyword"><?php echo $search;?></span>" 查無相關結果<br />更多熱門賽事推薦給您
                        <?php endif;?>
                    </h3>
				</div>
			</div>
            <?php endif;?>
			<div class="albumWrap">
                <?php if(count($albums)):?>
                    <?php foreach($albums as $k => $v):?>
                    <a class="albums" href="<?php echo $v["album_link"];?>">
                        <div class="album_img" style="background-image:url(<?php echo $v["album_cover"];?>);"></div>
                        <div class="album_info">
                            <h2 class="album_title"><?php echo $v["album_name"];?></h2>
                        </div>
                    </a>
                    <?php if(count($activitys) && isset($activitys[$k])):?>
                    <a class="albums ad" href="<?php echo $activitys[$k]["link"];?>">
                        <div class="album_img" style="background-image:url(<?php echo $activitys[$k]["img"];?>);"></div>
                        <div class="album_info">
                            <h2 class="album_title"></h2>
                        </div>
                    </a>
                    <?php endif;?>
                    <?php endforeach;?>
                <?php elseif(count($activitys)):?>
                    <?php foreach($activitys as $k => $v):?>
                    <a class="albums ad" href="<?php echo $v["link"];?>">
                        <div class="album_img" style="background-image:url(<?php echo $v["img"];?>);"></div>
                        <div class="album_info">
                            <h2 class="album_title"></h2>
                        </div>
                    </a>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
            <div id="loading">
				<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 75 75">
					<circle cx="37.5" cy="37.5" r="33.5"></circle>
				</svg>
			</div>
            <input type="hidden" id="offset" name="offset" value="<?php echo $offset;?>" />
            <input type="hidden" id="searchold" name="searchold" value="<?php echo $search;?>" />
            <input type="hidden" id="activity" name="activity" value="<?php echo $activity;?>" />
		</div>
		<?php echo $footer; ?>
	</div>
    <script type="text/javascript" src="catalog/view/javascript/search.js"></script>
</body>
</html>