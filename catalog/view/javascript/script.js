$(function(){
	$(window).scroll(function() {
		var dWindow = $(this);
		if ((dWindow).scrollTop() > 100) {
			$("#goTop").fadeIn();
		} else {
			$("#goTop").fadeOut();
		}
	});

	//menu toggle
	$(".iconToggle").click(function(){
		var $nav = $(".header nav");
		if ($nav.hasClass("active")){
			$nav.animate({left:'-180'});
			$nav.toggleClass("active");
		}else{
			$nav.animate({left:'0'});
			$nav.toggleClass("active");
		}
	});

	//Scroll To Top
	$("#goTop").click(function goTop(){
		$("html,body").animate({scrollTop:0},400);
	});

})