$(function(){
	$(window).scroll(function() {
		var dWindow = $(this);
		if ((dWindow).scrollTop() > 100) {
			$("#goTop").fadeIn();
		} else {
			$("#goTop").fadeOut();
		}
	});

	var $container = $('.albumWrap');

	//Images Loaded
	$container.imagesLoaded(function(){

		$container.masonry({
			itemSelector: '.albums'
		});

	});

	//photo
	var wContainer = -1;
	var wColumn = -1;
	var columnsInRow = -1;
	var currentColumn = -1;
	var currentRow = -1;
	var prevItem = -1;
	var lastItem = -1;
	var insertItem = -1;
	var row = -1;
	var column = -1;
	var $containerphoto = $('.photoWrap');
	var photosTrigger = $('.photosTrigger');
	var detailsBox = $('.detailsBox');
	var length = photosTrigger.length;

	photosTrigger.click(function() {
		if(length > 0){
			
			checkPos(this);

			if(column == -1){
				//first time
				detailsBox.insertAfter(photosTrigger.eq(insertItem)).slideToggle(300);
				$(this).addClass('current isExpanded');
				// console.log("first Time"); 
			}else{
				if (column == currentColumn) {
					//same column
					detailsBox.slideToggle(300);
					$(this).toggleClass('isExpanded');
					// console.log("same column");
				}else{
					var isExpanded = $('.current').hasClass('isExpanded');
					if(row != currentRow){
						//diffenent column & row
						detailsBox.slideUp(100,function(){
							detailsBox.insertAfter(photosTrigger.eq(insertItem)).slideDown(300);
						});
						// console.log("diff row diff column");	
					}else if(row == currentRow && !isExpanded){
						//diffenent column same row 
						detailsBox.slideDown(300);
						// console.log("isCollapse");
					}
					$('.current').removeClass('current isExpanded');
					$(this).addClass('current isExpanded');

				}
			}
			
			row = currentRow;
			column = currentColumn;
		}
	});

	$('.btn_close').click(function(){
		detailsBox.slideToggle(300);
		$('.current').toggleClass('isExpanded');
		// console.log('btn_close');
	});

	$(window).resize(function(){
		var current = $('.current');
		if (length > 0 && current.length > 0){
			var isExpanded = current.hasClass('isExpanded');
			
			if(isExpanded){
				checkPos(current);
				detailsBox.insertAfter(photosTrigger.eq(insertItem));
				// console.log('change insert to' + insertItem);
				// console.log('wContainer '+ wContainer + ",wColumn= " + wColumn + ",columnsInRow= "+columnsInRow);
			}
		}
	});

	function checkPos(current){
		wContainer = $containerphoto.width();
		wColumn = photosTrigger.eq(0).outerWidth(true);
		columnsInRow = parseInt(wContainer/wColumn);
		currentColumn = photosTrigger.index(current);
		currentRow = parseInt(currentColumn/columnsInRow);
		prevItem = (currentRow+1)*columnsInRow-1;
		lastItem = photosTrigger.length-1;

		if(prevItem <= lastItem){
			insertItem = prevItem;
		}else{
			insertItem = lastItem;
		}
	}

	//menu toggle
	$(".iconToggle").click(function(){
		var $nav = $(".header nav");
		if ($nav.hasClass("active")){
			$nav.animate({left:'-180'});
			$nav.toggleClass("active");
		}else{
			$nav.animate({left:'0'});
			$nav.toggleClass("active");
		}
	});

	//Scroll To Top
	$("#goTop").click(function goTop(){
		$("html,body").animate({scrollTop:0},400);
	});
})