<?php
class ControllerCommonHome extends Controller {
    /**
	 * [home 首頁]
	 * @return  [type]        [description]
	 * @Another Nicole
	 * @date    2018-03-29
	 */
	public function index() {
        $server = $this->config->get('serverLink');
        // 準備列表資料==========================================================================
        $this->load->model('catalog/activity');
        $this->load->model('catalog/album');
        
        $data["offset"] = 20;
        $data["activitys"] = array();
        
        $filterArr = array(
            "start"     => 0,
            "limit"     => 20
        );
        $activityArr = $this->model_catalog_activity->getLists($filterArr);
        if(count($activityArr)){
            // 相簿分類
            $albumArr = array();
            foreach($activityArr as $activity){
                if(empty($activity["album"])) continue;
                $albumArr[$activity["album"]] = ''; 
            }
            $coverArr = $this->model_catalog_album->getAlbumCovers(array_keys($albumArr));
            if(count($coverArr)){
                foreach($coverArr as $cover){
                    $albumArr[$cover["album_id"]] = empty($cover["cover_image"]) ? $cover["first_image"] : $cover["cover_image"]; 
                }
            }
            $this->load->model('tool/image');
            foreach($activityArr as $activity){
                $cover_image = '';
                if(!empty($albumArr[$activity["album"]])){
                    $cover_image = $this->model_tool_image->resizePhoto($albumArr[$activity["album"]]);
                }
                if(!$cover_image){
                    $cover_image = $server.'image/catalog/album_default.jpg';
                }
                $data["activitys"][] = array(
                    "activity_title"    => $activity["title"],
                    "activity_cover"    => $cover_image,
                    "activity_link"     => $this->url->link('common/search', 'activity=' . $activity["seqno"], true)
                );
            }
        }
        //echo "<pre>",print_r($data["activitys"],1),"</pre>";exit;
        $data["search_action"] = $this->url->link('common/search', '', true);
        
        // 設定 meta data==========================================================================
        $this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setImages($server.$this->config->get('config_meta_keyword'));
        
        // 程式最後 ==============================================================================
        $data['header']     = $this->load->controller('common/header');
        $data['header_bar'] = $this->load->controller('common/header/navBar');
        $data['footer']     = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/home', $data));
	}
    
    /**
	 * [ajaxActivitys 瀑布流資料]
	 * @return  [type]        [description]
	 * @Another Nicole
	 * @date    2018-03-29
	 */
    public function ajaxActivitys(){
        $server = $this->config->get('serverLink');
        
        // get 取回的資料=========================================================================
        $act    = isset($this->request->get['act']) ? $this->request->get['act'] : "";
        $offset = isset($this->request->get['offset']) ? intval($this->request->get['offset']) : 20;
        
        // 準備資料==============================================================================
        $responseHtml = '';
        if($act=='ajax'){
            $this->load->model('catalog/activity');
            $this->load->model('catalog/album');
            $filterArr = array(
                "start"     => $offset,
                "limit"     => 20
            );
            $activityArr = $this->model_catalog_activity->getLists($filterArr);
            if(count($activityArr)){
                // 相簿分類
                $albumArr = array();
                foreach($activityArr as $activity){
                    if(empty($activity["album"])) continue;
                    $albumArr[$activity["album"]] = ''; 
                }
                if(count($albumArr)){
                    $coverArr = $this->model_catalog_album->getAlbumCovers(array_keys($albumArr));
                    if(count($coverArr)){
                        foreach($coverArr as $cover){
                            $albumArr[$cover["album_id"]] = empty($cover["cover_image"]) ? $cover["first_image"] : $cover["cover_image"];
                        }
                    }
                }
                $this->load->model('tool/image');
                foreach($activityArr as $activity){
                    $cover_image = '';
                    if(!empty($albumArr[$activity["album"]])){
                        $cover_image = $this->model_tool_image->resizePhoto($albumArr[$activity["album"]]);
                    }
                    if(!$cover_image){
                        $cover_image = $server.'image/catalog/album_default.jpg';
                    }
                    $activity_link = $this->url->link('common/search', 'activity=' . $activity["seqno"], true);
                    $responseHtml .= '<a class="albums" href="'.$activity_link .'">';
                    $responseHtml .= '<div class="album_img" style="background-image:url('.$cover_image.')"></div>';
                    $responseHtml .= '<div class="album_info">';
                    $responseHtml .= '<h2 class="album_title">'.$activity["title"].'</h2>';
                    $responseHtml .= '</div>';
                    $responseHtml .= '</a>';
                }
            }
        }
        
        $this->response->setOutput($responseHtml);
    }
}
