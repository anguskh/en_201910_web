<?php
class ControllerCommonHeader extends Controller {
	public function index() {
        
        // META DATA
        $data['title']          = $this->document->getTitle();
		$data['description']    = $this->document->getDescription();
		$data['sitename']       = $this->config->get('config_name');
		$data['image']          = $this->document->getImages();
        
        return $this->load->view('common/header', $data);
	}
    
    public function navBar(){
        $data = array();
        return $this->load->view('common/header_bar', $data);
    }
}
