<?php
class ModelCatalogActivity extends Model{
    
    /**
	 * [getLists 首頁賽事列表]
	 * @param   array   $data [description]
	 * @return  [type]        [description]
	 * @Another Nicole
	 * @date    2018-04-19
	 */
	public function getLists($data = array()) {
        
        $SQLCmd = "SELECT ac.seqno,ac.title,max(ab.album_id) as album ".
            "FROM ev_activity ac JOIN oc_album ab on(ac.seqno=ab.activityseqno) ".
            " WHERE ab.status=2 GROUP BY ac.seqno ORDER BY ab.shoot_start desc" ;
            
        //$SQLCmd = "SELECT ac.seqno,ac.title,max(ab.album_id) as album ".
        //    "FROM ev_activity ac LEFT JOIN oc_album ab on(ac.seqno=ab.activityseqno and ab.status=2) ".
        //    " WHERE 1=1 GROUP BY ac.seqno ORDER BY ac.seqno desc" ;
        
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $SQLCmd;exit;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}
    
    /**
	 * [getDefaltList 取賽事列表]
	 * @param   array   $data [description]
	 * @return  [type]        [description]
	 * @Another Nicole
	 * @date    2018-04-19
	 */
    public function getDefaltList($data = array()){
        $filterArr = array();
        if(isset($data['sdate']) && !empty($data['sdate'])){
            $filterArr[] = "startdate<='".$this->db->escape($data['sdate'])."'";
        }
        if(isset($data['edate']) && !empty($data['edate'])){
            $filterArr[] = "enddate>='".$this->db->escape($data['edate'])."'";
        }
        if(isset($data['flag'])){
            $filterArr[] = "flag=".intval($data['flag']);
        }
        
        $SQLCmd = "SELECT * FROM ev_activity WHERE 1=1";
        if(count($filterArr)){
            $SQLCmd = "SELECT * FROM ev_activity WHERE ".implode(" AND ",$filterArr);
        }
        $SQLCmd .= " ORDER BY sort";
        
        if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $SQLCmd;exit;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
    }
}
