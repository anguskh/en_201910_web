<?php
class Pagination {
	public $total = 0;
	public $page = 1;
	public $limit = 20;
	public $num_links = 8;
	public $url = '';
	public $text_first = '|&lt;';
	public $text_last = '&gt;|';
	public $text_next = '&gt;';
	public $text_prev = '&lt;';

	public function render() {
		$total = $this->total;

		if ($this->page < 1) {
			$page = 1;
		} else {
			$page = $this->page;
		}

		if (!(int)$this->limit) {
			$limit = 10;
		} else {
			$limit = $this->limit;
		}

		$num_links = $this->num_links;
		$num_pages = ceil($total / $limit);

		$this->url = str_replace('%7Bpage%7D', '{page}', $this->url);

		$output = '<ul class="pagination">';

		if ($page > 1) {
			$output .= '<li><a href="' . str_replace(array('&amp;page={page}', '&page={page}'), '', $this->url) . '">' . $this->text_first . '</a></li>';
			
			if ($page - 1 === 1) {
				$output .= '<li><a href="' . str_replace(array('&amp;page={page}', '&page={page}'), '', $this->url) . '">' . $this->text_prev . '</a></li>';
			} else {
				$output .= '<li><a href="' . str_replace('{page}', $page - 1, $this->url) . '">' . $this->text_prev . '</a></li>';
			}
		}

		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);

				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}

				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}

			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					$output .= '<li class="active"><span>' . $i . '</span></li>';
				} else {
					if ($i === 1) {
					$output .= '<li><a href="' . str_replace(array('&amp;page={page}', '&page={page}'), '', $this->url) . '">' . $i . '</a></li>';
					} else {
						$output .= '<li><a href="' . str_replace('{page}', $i, $this->url) . '">' . $i . '</a></li>';
					}
				}
			}
		}

		if ($page < $num_pages) {
			$output .= '<li><a href="' . str_replace('{page}', $page + 1, $this->url) . '">' . $this->text_next . '</a></li>';
			$output .= '<li><a href="' . str_replace('{page}', $num_pages, $this->url) . '">' . $this->text_last . '</a></li>';
		}

		$output .= '</ul>';

		if ($num_pages > 1) {
			return $output;
		} else {
			return '';
		}
	}
    
    public function make() 
	{	
		$output = "" ;
		$total = $this->total;

		if ($this->page < 1) 
		{
			$page = 1;
		} 
		else 
		{
			$page = $this->page;
		}

		if(!(int)$this->limit) 
		{
			$limit = 10;
		} 
		else 
		{
			$limit = $this->limit;
		}
		
		$url = $this->url;
		
		$total_page = ceil($total/$limit);

		if($total > $page *  $limit)
		{
			$p_cnt = $page*$limit;
		}
		else
		{
			$p_cnt = $total;
		}
				
		$output .= '<div class="pagerWrap">';			

		# 同頁
		if($page == $total_page)
		{
			$next = $total_page;
		}
		else
		{
			$next = $page+1;
		}
		if($page == 1)
		{
			$before = 1;
		}
		else
		{
			$before = $page - 1;
		}
		
		# 第一頁
		$output .= '<a class="pager endMove '.(($page==1)?'locked':'').'" href="'.$url.'&page=1">&lt;&lt;&lt;</a>';	
		# 前一頁			
		$output .= '<a class="pager '.(($page == $before)?'locked':'').'" href="'.$url.'&page='.$before.'">&lt;</a>';	

		for($i=1;$i<=5;$i++)
		{
			$now = $page+$i-3;
				
			if($now > 0 && $now <= $total_page)
			{
				# 數字頁籤
				$output .= '<a class="pager '.(($now == $page)?'current':'').'" href="'.$url.'&page='.$now.'">'.$now.'</a>';
			}
		}
		
		# 下一頁
		$output .= '<a class="pager '.(($page == $next)?'locked':'').'" href="'.$url.'&page='.$next.'">&gt;</a>';
					
		# 最尾頁			
		$output .= '<li class="pager '.(($page == $total_page)?'locked':'').'" href="'.$url.'&page='.$total_page.'">&gt;&gt;&gt;</a>';	
		
		
		$output .= '</div>';
					
		return $output;
	}
}
