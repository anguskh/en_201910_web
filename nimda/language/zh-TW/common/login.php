<?php
// header
$_['heading_title']  = 'Energy管理平台';

// Text
$_['text_heading']   = 'Energy管理平台';
$_['text_login']     = 'Energy管理平台';
$_['text_forgotten'] = '忘記密碼';

// Entry
$_['entry_username'] = '使用者帐号';
$_['entry_password'] = '使用者密码';

// Button
$_['button_login']   = '登入';

// Error
$_['error_login']    = '使用者帐号或密码错误';
$_['error_token']    = 'Token Session無效,請重新登入';