<?php
// Text
$_['text_album']            = '相簿管理';
$_['text_product']          = '相片管理';
$_['text_order']            = '訂單管理';
$_['text_category']         = '分類管理';
$_['text_report']           = '報表管理';
$_['text_report_click']     = '相簿/相片點擊數';
$_['text_report_create']    = '相簿/相片建檔統計';
$_['text_report_sale']      = '相片銷售量';
$_['text_users']            = '權限管理';
$_['text_user']             = '使用者管理';
$_['text_user_group']       = '群組帳號';

//運動咖
$_['text_sports']           = '運動咖管理';
$_['text_sports_article']   = '文章管理';
$_['text_sports_search']    = '熱門搜尋';
$_['text_sports_activity']  = '推薦賽事';