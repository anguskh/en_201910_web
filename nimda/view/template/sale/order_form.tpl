<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <?php if($pay_status>1){?>
                <button type="button" id="button-save" form="form-order" formaction="<?php echo $save; ?>" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <?php }?>
                <?php if($order_status==1 && $pay_status==1){?>
                <button type="button" id="button-delete" form="form-order" formaction="<?php echo $delete; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                <?php }?>
                <a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" id="form-order" method="post" action="">
                    <input type="hidden" name="order_id" value="<?php echo $order_id;?>">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th width="20%" class="text-center">訂單日期</th>
                                <td class="text-left"><?php echo date("Y-m-d",strtotime($date_added));?></td>
                            </tr>
                            <tr>
                                <th class="text-center">訂單編號</th>
                                <td class="text-left"><?php echo $order_number;?></td>
                            </tr>
                            <tr>
                                <th class="text-center">訂單狀態</th>
                                <td class="text-left">
                                    <?php 
                                    if($order_status==2){
                                        echo '<span style="color:red">'.$order_status_name.'</span>';
                                    }
                                    else{
                                        echo $order_status_name;
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">訂單金額</th>
                                <td class="text-left"><?php echo '$'.ceil($price);?></td>
                            </tr>
                            <tr>
                                <th class="text-center">點數折抵</th>
                                <td class="text-left"><?php echo $use_points.' 點';?></td>
                            </tr>
                            <tr>
                                <th class="text-center">付款方式</th>
                                <td class="text-left"><?php echo (isset($pay_types[$pay_type]) ? $pay_types[$pay_type] : $pay_type);?></td>
                            </tr>
                            <tr>
                                <th class="text-center">付款狀態</th>
                                <td class="text-left">
                                    <?php 
                                    if($pay_status==2){
                                        echo '<span style="color:blue">'.(isset($pay_statuses[$pay_status]) ? $pay_statuses[$pay_status] : $pay_status).'</span>';
                                    }
                                    else{
                                        echo (isset($pay_statuses[$pay_status]) ? $pay_statuses[$pay_status] : $pay_status);
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">會員帳號</th>
                                <td class="text-left"><?php echo $customer_acc;?></td>
                            </tr>
                            <tr>
                                <th class="text-center">會員姓名</th>
                                <td class="text-left"><?php echo $customer_name;?></td>
                            </tr>
                            <?php if($pay_status==2){?>
                            <tr>
                                <th class="text-center">下載效期</th>
                                <td class="text-left">
                                    <div class="col-sm-4">
                                        <div class="input-group date">
                                            <input type="text" name="date_start" value="<?php echo $date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group date">
                                            <input type="text" name="date_end" value="<?php echo $date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php }?>
                            <tr>
                                <th class="text-center">維護人員</th>
                                <td class="text-left"><?php echo $modify_user;?></td>
                            </tr>
                            <tr>
                                <th class="text-center">維護時間</th>
                                <td class="text-left"><?php echo $date_modified;?></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php if(count($order_products)){?>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="20%" class="text-center">相片編號</th>
                                <th class="text-center">相片</th>
                                <th class="text-center">相簿名稱</th>
                                <th class="text-center">金額</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($order_products as $k => $v){?>
                            <tr>
                                <td class="text-center"><?php echo $v["product_id"];?></td>
                                <td class="text-center"><img src="<?php echo $v["image"];?>" class="img-thumbnail" /></td>
                                <td class="text-center"><?php echo $v["album"];?></td>
                                <td class="text-center"><?php echo '$'.ceil($v["price"]);?></td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                    <?php }?>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });
    $('#button-delete').on('click', function(e) {
        $('#form-order').attr('action', this.getAttribute('formAction'));
        
        if (confirm('<?php echo $text_confirm; ?>')) {
            $('#form-order').submit();
        } else {
            return false;
        }
    });
    $('#button-save').on('click', function(e) {
        $('#form-order').attr('action', this.getAttribute('formAction'));
        $('#form-order').submit();
    });
    </script> 
</div>
<?php echo $footer; ?> 