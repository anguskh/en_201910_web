<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="javascript:void(0);" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary" onclick="checkSubmit();"><i class="fa fa-save"></i></a>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-album" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-number"><?php echo $column_number; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="number" value="<?php echo (empty($number) ? '系統預設' : $number);?>" placeholder="<?php echo $column_number; ?>" id="input-number" class="form-control" disabled />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-activity"><?php echo $column_activity; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="activity" value="<?php echo $activity; ?>" placeholder="賽事名稱" id="input-activity" class="form-control" />
                            <input type="hidden" name="activityseqno" value="<?php echo $activityseqno; ?>" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-name"><?php echo $column_name; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="name" value="<?php echo $name; ?>" placeholder="賽事名稱+攝影師" id="input-name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-start"><?php echo $column_start; ?></label>
                        <div class="col-sm-5">
                            <div class="input-group date" id="sdate">
                                <input type="text" name="shoot_start" value="<?php echo $shoot_start; ?>" placeholder="<?php echo $column_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="input-group date" id="edate">
                                <input type="text" name="shoot_end" value="<?php echo $shoot_end; ?>" placeholder="<?php echo $column_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-tag"><?php echo $column_tag; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="tag" value="<?php echo $tag; ?>" id="input-tag" data-role="tagsinput"  class="form-control" />
                        </div>
                    </div>
                    <?php if(!empty($status)){?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-tag"><?php echo $column_status; ?></label>
                        <div class="col-sm-10">
                            <select name="status" class="form-control">
                                <?php 
                                foreach($statusArr as $k => $v){
                                    if(!$k) continue;
                                ?>
                                <option value="<?php echo $k; ?>" <?php if($k==$status){?>selected<?php }?>><?php echo $v; ?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <?php }?>
                    <?php if(!empty($album_id)){?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-image"><?php echo $column_cover; ?></label>
                        <div class="col-sm-10">
                            <a href="javascript:void(0);" id="img-change" class="img-thumbnail"><img src="<?php echo $cover_image; ?>" data-placeholder="<?php echo $cover_image; ?>"/></a>
                            <input type="hidden" name="cover_id" value="<?php echo $cover_id; ?>" data-placeholder="<?php echo $cover_id; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-editor"><?php echo $column_last_edit; ?></label>
                        <div class="col-sm-10" style="padding-top:9px;"><?php echo $editor_name; ?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-edit-time"><?php echo $column_last_edit_time; ?></label>
                        <div class="col-sm-10" style="padding-top:9px;"><?php echo $editor_time; ?></div>
                    </div>
                    <?php }?>
                </form>
            </div>
        </div>
  </div>
</div>
<?php echo $footer; ?> 
<link href="view/stylesheet/bootstrap-tagsinput.css" type="text/css" rel="stylesheet" />
<script src="view/javascript/bootstrap-tagsinput.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#form-album').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
            return false;
        }
    });
    $('#sdate,#edate').datetimepicker({
        pickTime: false
    });
    $('.img-thumbnail').on('click',function(e){
        var $element = $(this);
        if ($element.data('bs.popover')) {
			return;
        }
		$element.popover({
			html: true,
			placement: 'right',
			trigger: 'manual',
			content: function() {
				return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
                //return '<button type="button" id="button-upload" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
			}
		});
		$element.popover('show');
        
        $('#button-image').on('click', function() {
			var $button = $(this);
			var $icon   = $button.find('> i');
			
			$('#modal-image').remove();
            
            $.ajax({
				url: 'index.php?route=album/album&token=' + getURLVar('token') + '&type=image&album_id=<?php echo $album_id?>',
				dataType: 'html',
				beforeSend: function() {
					$button.prop('disabled', true);
					if ($icon.length) {
						$icon.attr('class', 'fa fa-circle-o-notch fa-spin');
					}
				},
				complete: function() {
					$button.prop('disabled', false);
					if ($icon.length) {
						$icon.attr('class', 'fa fa-pencil');
					}
				},
				success: function(html) {
                    console.log(html);
                    if(html){
                        $('body').append('<div id="modal-image" class="modal">' + html + '</div>');
                        $('.modal-title').append(' - '+$('[name="name"]').val());
                        $('#modal-image').modal('show');
                    }
                    else{
                        alert('相簿資料不存在！');
                    }
				}
			});
            
            $element.popover('destroy');
        });
        
        $('#button-clear').on('click', function() {
			$element.find('img').attr('src', $element.find('img').attr('data-placeholder'));
			$element.parent().find('input').attr('value', $element.parent().find('input').attr('data-placeholder'));
			$element.popover('destroy');
		});
    });
    
    var activityName = '';
    // 賽事
    $('input[name=\'activity\']').autocomplete({
        'source': function(request, response) {
            if(activityName==request){
                return false;
            }
            activityName = request;
            $('input[name=\'activityseqno\']').val('');
            $.ajax({
                url: 'index.php?route=album/album/ajaxActivity&token=<?php echo $token; ?>&name=' +  encodeURIComponent(request),
                dataType: 'json',			
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'activity\']').val(item['label']);
            $('input[name=\'name\']').val(item['label']);
            $('input[name=\'activityseqno\']').val(item['value']);
    
        }	
    });
});
function checkSubmit(){
    var err = new Array();
    //var number = $('[name="album_number"]').val();
    //if($.trim(number).length<=6 || $.trim(number).length>30){
    //    err[err.length] = "相簿編號為6~30個字";
    //}
    var seqno = $('[name="activityseqno"]').val();
    if(seqno==0 || seqno==''){
        err[err.length] = "請輸入正確的賽事名稱";
    }
    var name = $('[name="name"]').val();
    if($.trim(name).length<=6 || $.trim(name).length>100){
        err[err.length] = "相簿名稱為6~100個字";
    }
    var start = $('[name="shoot_start"]').val();
    var end = $('[name="shoot_end"]').val();
    if(start.length<=0 || end.length<=0){
        err[err.length] = "請輸入拍攝日期";
    }
    else if(start == '0000-00-00' || end == '0000-00-00'){
        err[err.length] = "拍攝日期格式錯誤";
    }
    else if(!/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(start) || !/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(end)){
        err[err.length] = "拍攝日期格式錯誤";
    }
    else if(Date.parse(start).valueOf() > Date.parse(end).valueOf()){
        err[err.length] = "拍攝日期開始時間不可超過結束時間";
    }
    
    if(err.length>0){
        alert(err.join('\n'));
        return false;
    }
    else{
        $('#form-album').submit();
    }
}
</script>