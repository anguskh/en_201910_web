<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="javascript:void(0);" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary" onclick="checkSubmit();"><i class="fa fa-save"></i></a>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-article" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $columnName; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="keywords" value="<?php echo $keywords; ?>" data-role="tagsinput"  class="form-control" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
  </div>
</div>
<?php echo $footer; ?> 
<link href="view/stylesheet/bootstrap-tagsinput.css" type="text/css" rel="stylesheet" />
<script src="view/javascript/bootstrap-tagsinput.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#form-article').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
            return false;
        }
    });
});
function checkSubmit(){
    var err = new Array();
    
    if(err.length>0){
        alert(err.join('\n'));
        return false;
    }
    else{
        $("#form-article").submit();
    }
}
</script>