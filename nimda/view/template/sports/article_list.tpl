<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_publish; ?>" class="btn btn-danger" onclick="checkPublish(1);"><i class="fa fa-arrow-up"></i></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="checkPublish(2);"><i class="fa fa-arrow-down"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
            	<div class="well">
            		<div class="row">
            			<div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-sdate"><?php echo $searchNames["sdate"]; ?></label>
                                <div class="input-group date" id="sdate">
                                    <input type="text" name="sdate" value="<?php echo $sdate; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-edate"><?php echo $searchNames["edate"]; ?></label>
                                <div class="input-group date" id="edate">
                                    <input type="text" name="edate" value="<?php echo $edate; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-title"><?php echo $searchNames["title"]; ?></label>
                                <input type="text" name="title" value="<?php echo $title; ?>" id="input-title" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label"><?php echo $searchNames["source"]; ?></label>
                                <input type="text" name="source" value="<?php echo $source; ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $searchNames["status"]; ?></label>
                                <select name="status" class="form-control">
                                    <option value="">請選擇</option>
                                    <option value="1" <?php if($status==1):?>selected<?php endif;?>>上架</option>
                                    <option value="2" <?php if($status==2):?>selected<?php endif;?>>下架</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $searchNames["limit"]; ?></label>
                                <select name="limit" class="form-control">
                                    <option value="20" <?php if($limit==20):?>selected<?php endif;?>>20筆</option>
                                    <option value="50" <?php if($limit==50):?>selected<?php endif;?>>50筆</option>
                                    <option value="100" <?php if($limit==100):?>selected<?php endif;?>>100筆</option>
                                </select>
                            </div>
                            <button type="button" id="button-clear" class="btn btn-danger pull-right" style="margin-left:5px;"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
                        </div>
            		</div>
            	</div>
				<form action="<?php echo $publish; ?>" method="post" enctype="multipart/form-data" id="form-atricle">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<td style="width: 1px;" class="text-center">
									<input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
								</td>
								<?php foreach ($columnNames as $key => $colName) : ?>
                                <td class="text-center">
                                    <a href="javascript:void(0);" class=""><?php echo $colName; ?></a>
                                </td>
								<?php endforeach ; ?>
                                <td width="15%" class="text-center"><?php echo $column_tags; ?></td>
                                <td width="10%" class="text-center"><?php echo $column_counter; ?></td>
                                <td class="text-center"><?php echo $column_action; ?></td>
							</tr>
						</thead>
						<tbody>
                        <?php if(count($results)) :?>
						<?php foreach ( $results as $iCnt => $tmpRow) : ?>
							<tr>
								<td class="text-center">
									<input type="checkbox" name="selected[]" value="<?php echo $tmpRow['seqno']; ?>" />
								</td>
							<?php foreach ($columnNames as $key => $colName) : ?>
								<td class="text-center"><?php echo $tmpRow[$key]; ?></td>
							<?php endforeach ; ?>
                                <td class="text-center">
									<?php echo implode("、",$tags[$tmpRow["seqno"]]);?>
								</td>
                                <td class="text-center">
                                    <?php if($clicks[$tmpRow["seqno"]]['webCnt']>0 || $clicks[$tmpRow["seqno"]]['mobileCnt']>0):?>
									<a href="javascript:void(0);" onclick="showDetail(this,'<?php echo $tmpRow["seqno"];?>');"><?php echo $clicks[$tmpRow["seqno"]]['webCnt']." / ".$clicks[$tmpRow["seqno"]]['mobileCnt'];?></a>
                                    <?php else:?>
                                    <?php echo $clicks[$tmpRow["seqno"]]['webCnt']." / ".$clicks[$tmpRow["seqno"]]['mobileCnt'];?>
                                    <?php endif;?>
								</td>
								<td class="text-center">
									<a href="<?php echo $tmpRow['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
								</td>
							</tr>
						<?php endforeach ; ?>
                        <?php else :?>
                        <tr>
                            <td class="text-center" colspan="<?php echo count($columnNames)+4;?>"><?php echo $text_no_results; ?></td>
                        </tr>
                        <?php endif ;?>
						</tbody>
						</table>
					</div>
				</form>
				<div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $indexDesc; ?></div>
                </div>
            </div>
        </div>
        <?php echo $modal;?>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#sdate,#edate').datetimepicker({
        pickTime: false
    });
    $('#button-filter').on('click', function() {
        var url = 'index.php?route=sports/article&token=<?php echo $token; ?>';
        var sdate = $('input[name=\'sdate\']').val();
        if (sdate) {
            url += '&sdate=' + encodeURIComponent(sdate);
        }
        var edate = $('input[name=\'edate\']').val();
        if (edate) {
            url += '&edate=' + encodeURIComponent(edate);
        }
        var title = $('input[name=\'title\']').val();
        if (title) {
            url += '&title=' + encodeURIComponent(title);
        }
        var source = $('[name=\'source\']').val();
        if (source) {
            url += '&source=' + encodeURIComponent(source);
        }
        var status = $('[name=\'status\']').val();
        if (status) {
            url += '&status=' + encodeURIComponent(status);
        }
        var limit = $('[name=\'limit\']').val();
        if (limit) {
            url += '&limit=' + encodeURIComponent(limit);
        }
        location = url;
    });
    $("#button-clear").click(function(){
        $('input[name="sdate"]').val('');
        $('input[name="edate"]').val('');
        $('input[name="title"]').val('');
        $('input[name="source"]').val('');
        $('input[name="status"]').val('');
        $('input[name="limit"]').val(20);
    });
});
function checkPublish(type){
    var type_name = type==1 ? '上架' : '下架';
    var confirmMsg = '';
    $('[name="selected[]"]').each(function(){
        console.log($(this).prop("checked"));
        if($(this).prop("checked")){
            confirmMsg += $(this).closest('tr').find('td:eq(2)').text()+"\n";
        }
    });
    if(confirmMsg.length<=0){
        alert("請先勾選要"+type_name+"的文章！");
    }
    else {
        if(confirm("確定要"+type_name+"下列文章？\n\n"+confirmMsg)){
            $("#form-atricle").append('<input type="hidden" name="type" value="'+type+'">');
            $("#form-atricle").submit();
        }
    }
}
function showDetail(obj,article){
    var title = $(obj).closest('tr').find('td:eq(2)').text();
    $(".modal-title").html(title+' ─文章點擊歷程');
    $("#modal_table tbody").html("<tr><td colspan=\"3\">無資料</td></tr>");
    $.ajax({
		url: 'index.php?route=sports/article/ajaxClick&token=<?php echo $token;?>',
		type: 'get',		
		dataType: 'json',
		data:{article:article},
		cache: false,
		success: function(resp) {
			if(resp.length>0){
                var htmlTxt = '';
                for(var i=0;i<resp.length;i++){
                    htmlTxt += '<tr>';
                     htmlTxt += '<td>'+(i+1)+'</td>';
                    htmlTxt += '<td>'+resp[i].time+'</td>';
                    htmlTxt += '<td>'+resp[i].agent+'</td>';
                    htmlTxt += '</tr>';
                }
                $("#modal_table tbody").html(htmlTxt);
            }
            $("#article_modal").modal('show');
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            $("#article_modal").modal('show');
		}
	});
}
</script>
<?php echo $footer; ?>