<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <!--a href="<?php echo $repair; ?>" data-toggle="tooltip" title="<?php echo $button_rebuild; ?>" class="btn btn-default"><i class="fa fa-refresh"></i></a-->
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_parent; ?></label>
                                <select name="parent" class="form-control">
                                    <option value="">請選擇</option>
                                    <?php foreach($parentArr as $k => $v){?>
                                    <option value="<?php echo $v["category_id"];?>" <?php if($parent==$v["category_id"]){?>selected<?php }?>><?php echo $v["name"];?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td width="5%" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                <td class="text-left">
                                    <a href="<?php echo $sort_name; ?>" class="<?php echo ($sort=='name' ? strtolower($order):''); ?>"><?php echo $column_name; ?></a>
                                </td>
                                <!--td class="text-right">
                                    <a href="<?php echo $sort_sort_order; ?>" class="<?php echo ($sort=='sort_order' ? strtolower($order):''); ?>"><?php echo $column_sort_order; ?></a>
                                </td-->
                                <td class="text-right">
                                    <a href="<?php echo $sort_status; ?>" class="<?php echo ($sort=='status' ? strtolower($order):''); ?>"><?php echo $column_status; ?></a>
                                </td>
                                <td class="text-right"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($categories) { ?>
                            <?php foreach ($categories as $category) { ?>
                            <tr>
                                <td class="text-center"><?php if (in_array($category['category_id'], $selected)) { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
                                    <?php } else { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" />
                                    <?php } ?></td>
                                <td class="text-left"><?php echo $category['name']; ?></td>
                                <!--td class="text-right"><?php echo $category['sort_order']; ?></td-->
                                <td class="text-right"><?php echo ($category['status']==1 ? '啟用' : '停用'); ?></td>
                                <td class="text-right"><a href="<?php echo $category['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
$(document).ready(function(){
    $('#button-filter').on('click', function() {
        var url = 'index.php?route=catalog/category&token=<?php echo $token; ?>';
        var name = $('input[name=\'name\']').val();
        if (name) {
            url += '&name=' + encodeURIComponent(name);
        }
        var parent = $('[name=\'parent\']').val();
        if (parent) {
            url += '&parent=' + encodeURIComponent(parent);
        }
        location = url;
    });
});
</script>
